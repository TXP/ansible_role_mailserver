Ansible Mailserver Role
=========

An Ansible role to deploy a full featured mailserver with postfix, dovecot and
amavisd.

Requirements
------------

None

Role Variables
--------------

None.

Dependencies
------------

None.

Example Playbook
----------------

User the role as follows:

```yaml
  - hosts: mailservers
    roles:
      - role: mailserver
```

License
-------

AGPLv3

Author Information
------------------

Rob Connolly [https://webworxshop.com](https://webworxshop.com)
